"""
ghlight LineNr ctermfg=grey ctermbg=white

FDE Solution, SS TP Project
Authors: Blaise Delaney and Conor Davis
Supervisor: Stefan Hutzler
Thanks to Fritz Dunne for techical assistance
"""


import numpy as np
import math as mt #very sloppy
import matplotlib.pyplot as plt
from datetime import datetime

start_time = datetime.now()

#open file to pipe results - presumably this is for the plotting with Gnuplot?


#specify time (seconds), foam height (metres), number of output profiles
totaltime = 100
col_height = 6.0 
out_profiles = 50

#specify initial uniform liquid fraction( (LF), base LF, input LF at the base (?), critical LF for bubble rupture/
initial_lf = 0 #set to a constant we need 
base_lf = 0.36 
input_lf = 0.36 
crit_lf = 0.005 
sqrt_fl = 0.0 #square flow rate

shift_val = 5 #shift value 

#Step sizes 
dt = 0.001
dz = 0.01
dz1 = 1.0/dz
rz1 = dt*dz1

#Array for Plateau Border (PB) dimensionless area, alpha(z, t)
z_height = int(col_height/dz)
foam_height = 0
firstdim = (z_height+ 1)
seconddim = 2
alpha = np.ones((firstdim, seconddim)) #matrix dim = fractional height x 2
q = np.zeros(alpha[:,0].size)

maxtime = int(float(totaltime/dt))

profiles = 20 #There is a .dat file in the .C code that I don't understand. Set to 20 to work with a constant.
freq = int(float(maxtime/profiles)) #number of steps before output is written

#0.36 packing fraction for bubbles at bottom, constant
alpha[0][1] = base_lf


#BC: base_lf at bottom, and initial_lf elsewhere
for z in range(z_height+1):
    
    if z==0:
        alpha[z][0] = base_lf #liqud fraction at the basis
    
    else: #elsewhere the foam is as 'dry' as specified in IC
        alpha[z][0] = initial_lf
        alpha[z][1] = initial_lf
         
for i in range (1, maxtime+1): #time-evolve the system (~10^7)
    
    t = float(i*dt) #this is what is causing the code to run so slowly
    
    nc = (i+1)%2 #nc and np alternate between columns of alpha (previous step and next step)
    nl = i%2 #used to cycle between the two dimensions of alpha
    
    if i%2000 == 0:
        
        for j in range(shift_val, z_height+1):
            alpha[j][nc] = alpha[j-shift_val][nl]

        for j in range(1, shift_val):
            alpha[j][nc] = input_lf    
    

    print 'qs', np.shape(q), np.shape(q[:-1])
    print 'as', np.shape(alpha), np.shape(alpha[1:,nc])    
    q[:-1] = alpha[1:,nc] * alpha[1:,nc] + np.sqrt(alpha[1:,nc]) * 0.5 * np.ediff1d(alpha[:,nc]) * dz1
    q[-1] = sqrt_fl
    alpha[1:,nl] = alpha[1:,nc] - rz1 * ( np.ediff1d(q) )
    print 'sanity check'

    print alpha[:10,nl]
    

    for j in range(1, z_height+1):
        #rupture condition/no bubbles
        if alpha[j][nl] <= crit_lf:
            alpha[j][nl] = initial_lf
        

        if alpha[j][nl] > initial_lf: 
            foam_height = (j+1)*dz
    
    if i%freq==0 and (i>freq):

        np.savetxt('profile_' + str(t) + '_' + str(foam_height) + '.dat', np.array(alpha[:, nl]))

end_time = datetime.now()
print('Duration: {}'.format(end_time - start_time))

