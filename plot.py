#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt('out_Rise.dat', dtype = 'float', delimiter="\t")
plt.plot(data[:,-2], data[:,-1], 'k^')
plt.grid()
plt.ylabel('Height')
plt.xlabel('Time')
plt.show()
